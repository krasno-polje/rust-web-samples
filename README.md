# Requirements

Cargo package manager. I recommend to install it via [rustup](https://rustup.rs).

# Compilation

```bash
cargo build --release
```

The executable file will be located in ./target/release/[project-name].
