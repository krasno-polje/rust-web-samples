use std::str::FromStr;

use r2d2_sqlite::rusqlite::ErrorCode;
use rusqlite::{params, ToSql, Row};

use crate::entity::account::{UserRole, Account};

use super::get_connection;

#[cfg(test)]
mod tests;

pub fn create_table() {
    let con = get_connection();
    let sql = r#"
        CREATE TABLE IF NOT EXISTS account (
            id            INTEGER PRIMARY KEY,
            name          TEXT NOT NULL UNIQUE,
            password_hash TEXT NOT NULL UNIQUE,
            role          TEXT NOT NULL DEFAULT 'User'
        )
    "#;
    con.execute(sql, params![]).unwrap();
}

#[derive(Debug)]
pub enum CreateError {
    AlreadyExists,
    OtherSqlError,
    SystemError
}

#[derive(Debug)]
pub enum UpdateError {
    NotExists
}

pub fn create_with_role(username: &str, password_hash: &str, role: &UserRole) -> Result<Account, CreateError> {
    let con = get_connection();
    let mut stmt = con.prepare(
        "INSERT INTO account (name, password_hash, role) VALUES (?, ?, ?)").unwrap();
    stmt.insert(params![username, password_hash, role.to_string()])
        .map(|id| {
            Account::new(id, username, password_hash, &role)
        })
        .map_err(|full_err| {
            match &full_err {
                rusqlite::Error::SqliteFailure(err, _) => {
                    match err.code {
                        ErrorCode::ConstraintViolation => CreateError::AlreadyExists,
                        _ => CreateError::OtherSqlError
                    }
                },
                _ => CreateError::SystemError
            }
        })
}

pub fn create(username: &str, password_hash: &str) -> Result<Account, CreateError> {
    create_with_role(username, password_hash, &UserRole::User)
}

fn delete(sql: &str, params: &[&dyn ToSql]) -> bool {
    let con = get_connection();
    let mut stmt = con.prepare(sql).unwrap();
    match stmt.execute(params).ok() {
        Some(deleted_count) => deleted_count != 0,
        None => false
    }
}

pub fn delete_by_id(id: i64) -> bool {
    let sql = "DELETE from account WHERE id = ?";
    delete(sql, params![id])
}

pub fn delete_by_name(name: &str) -> bool {
    let sql = "DELETE from account WHERE name = ?";
    delete(sql, params![name])
}

pub fn delete_all() {
    let con = get_connection();
    con.execute("DELETE from account", params![]).unwrap();
}

pub fn exists_by_name(name: &str) -> bool {
    let con = get_connection();
    let sql = "SELECT count(*) > 0 FROM account WHERE name = ?";
    let mut stmt = con.prepare(sql).unwrap();
    stmt.query_row(params![name], |row| {
        let exists: bool = row.get(0).unwrap();
        Ok(exists)
    }).unwrap()
}

fn map_row_to_struct(row: &Row) -> rusqlite::Result<Account> {
    let id: i64               = row.get("id").unwrap();
    let name: String          = row.get("name").unwrap();
    let password_hash: String = row.get("password_hash").unwrap();
    let role_str: String      = row.get("role").unwrap();
    let accont = Account::new(id, &name, &password_hash, 
        &UserRole::from_str(&role_str).unwrap());
    Ok(accont)
}

pub fn get_by_name(name: &str) -> Option<Account> {
    let con = get_connection();
    let sql = "SELECT * FROM account WHERE name = ?";
    let mut stmt = con.prepare(sql).unwrap();
    stmt.query_row(params![name], map_row_to_struct)
        .ok()
}

pub fn get_all() -> Vec<Account> {
    let con = get_connection();
    let sql = "SELECT * FROM account";
    let mut stmt = con.prepare(sql).unwrap();
    stmt.query_map(params![], map_row_to_struct)
        .unwrap()
        .map(|account| account.unwrap())
        .collect()
}


/// Returns false if an account with such a name does not exist
pub fn set_role_by_username(username: &str, role: &UserRole) -> bool {
    let con = get_connection();
    let sql = "UPDATE account SET role = ? WHERE name = ?";
    let mut stmt = con.prepare(sql).unwrap();
    match stmt.execute(params![role.to_string(), username]) {
        Ok(rows_updated) => rows_updated == 1,
        _ => false
    }
}

pub fn get_account_role_by_name(username: &str) -> Option<UserRole> {
    let con = get_connection();
    let sql = "SELECT role FROM account WHERE name = ?";
    let mut stmt = con.prepare(sql).unwrap();
    stmt.query_row(params![username], |row| {
        let role_str: String = row.get("role").unwrap();
        Ok(UserRole::from_str(&role_str).unwrap())
    })
    .ok()
}