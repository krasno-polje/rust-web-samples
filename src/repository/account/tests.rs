use crate::{
    repository::{
        init_db, 
        account::get_account_role_by_name
    }, 
    entity::account::{
        UserRole, 
        Account
    }, 
};

use super::{delete_all, create_with_role, get_all, set_role_by_username};

#[test]
fn test_create_with_role() {
    let username = "biciulis";
    let password = "sample password";
    let role = UserRole::Admin;
    let create_result = create_with_role(username, password, &role);
    match &create_result {
        Ok(account) => {
            dbg!(account);
        },
        Err(err) => {
            dbg!(err);
        }
    }
}

#[test]
fn test_delete_all() {
    delete_all();
}

#[test]
fn test_create_account_table() {
    init_db();
}

#[test]
fn test_get_all_accounts() {
    let accounts: Vec<Account> = get_all();
    dbg!(&accounts);
}

#[test]
fn test_account_set_role_by_username() {
    let username = "laisas";
    let role_changed: bool = set_role_by_username(username, &UserRole::User);
    dbg!(role_changed);
}

#[test]
fn test_get_account_role_by_name() {
    let username = "laisas";
    dbg!(get_account_role_by_name(username));
}