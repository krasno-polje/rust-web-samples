use std::sync::{Mutex, Arc};

use r2d2::{Pool, PooledConnection};
use r2d2_sqlite::SqliteConnectionManager;

pub mod person;
pub mod account;
pub mod personal_record;

#[cfg(test)]
mod tests;

pub enum EntityError {
    AlreadyExists, NotExist
}

pub struct RepoContext {
    pool: Pool<SqliteConnectionManager>
}

impl RepoContext {
    fn new() -> Self {
        Self { pool: create_pool() }
    }
}

fn create_pool() -> Pool<SqliteConnectionManager> {
    let conn_manager = SqliteConnectionManager::file("base.db");
    r2d2::Pool::new(conn_manager).unwrap()
}

static REPO_CONTEXT: Mutex<Option<Arc<RepoContext>>> = Mutex::new(None);

fn get_context() -> Arc<RepoContext> {
    let mut ctx_option = REPO_CONTEXT.lock().unwrap();
    match &*ctx_option {
        Some(ctx) => Arc::clone(ctx),
        None => {
            let ctx_ref: &Arc<RepoContext> = ctx_option.insert(Arc::new(RepoContext::new()));
            Arc::clone(ctx_ref)
        }
    }
}

fn get_connection() -> PooledConnection<SqliteConnectionManager> {
    let connection = get_context()
        .pool
        .get()
        .unwrap();
    connection.pragma_update(
        None, "foreign_keys", "ON").unwrap();
    connection
}

pub fn init_db() {
    person::create_table();
    account::create_table();
    personal_record::create_table();
}