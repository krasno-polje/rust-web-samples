use rusqlite::{
    params, 
    Error::SqliteFailure
};

use crate::{
    repository::get_connection, 
    entity::personal_record::PersonalRecord
};

use super::get_all;

#[test]
fn tmp_test_create_record() {
    let owner_id: i64 = 123;
    let message = "Некая личная запись";
    let con = get_connection();
    let sql = "INSERT INTO personal_record (owner_id, message) VALUES (?, ?)";
    let mut stmt = con.prepare(sql).unwrap();
    match stmt.insert(params![owner_id, message]) {
        Ok(generated_id) => {
            dbg!(generated_id);
        },
        Err(SqliteFailure(err, msg)) => {
            dbg!(&err);
            if let Some(msg) = msg {
                if msg.starts_with("FOREIGN KEY") {
                    println!("Foreign key violated!");
                }
            }
        },
        _ => {}
    }
}

#[test]
fn test_get_all() {
    let records: Vec<PersonalRecord> = get_all();
    dbg!(&records);
}