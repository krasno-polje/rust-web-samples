use rusqlite::{params, Error::SqliteFailure, Row, ToSql};

use crate::entity::personal_record::PersonalRecord;

use super::get_connection;

#[cfg(test)]
mod tests;

pub fn create_table() {
    let con = get_connection();
    let sql = r#"
        CREATE TABLE IF NOT EXISTS personal_record (
            id INTEGER PRIMARY KEY,
            owner_id INTEGER NOT NULL,
            message TEXT NOT NULL,
            FOREIGN KEY(owner_id) REFERENCES account(id) ON DELETE CASCADE
        )
    "#;
    con.execute(sql, params![]).unwrap();
}

pub enum CreateRecordError {
    OwnerNotExists,
    Other
}

pub fn create(owner_id: i64, message: &str) -> Result<PersonalRecord, CreateRecordError> {
    let con = get_connection();
    let sql = "INSERT INTO personal_record (owner_id, message) VALUES (?, ?)";
    let mut stmt = con.prepare(sql).unwrap();
    match stmt.insert(params![owner_id, message]) {
        Ok(generated_id) => Ok(PersonalRecord::new(generated_id, owner_id, message)),
        Err(SqliteFailure(_, msg)) => {
            if let Some(msg) = msg {
                if msg.starts_with("FOREIGN KEY") {
                    return Err(CreateRecordError::OwnerNotExists);
                }
            }
            Err(CreateRecordError::Other)
        },
        _ => Err(CreateRecordError::Other)
    }
}

fn map_row_to_struct(row: &Row) -> rusqlite::Result<PersonalRecord> {
    let id: i64         = row.get("id").unwrap();
    let owner_id: i64   = row.get("owner_id").unwrap();
    let message: String = row.get("message").unwrap();
    Ok(PersonalRecord::new(id, owner_id, &message))
}

fn get_multiple(sql: &str, params: &[&dyn ToSql]) -> Vec<PersonalRecord> {
    let con = get_connection();
    let mut stmt = con.prepare(sql).unwrap();
    stmt.query_map(params, map_row_to_struct)
        .unwrap()
        .map(|record| record.unwrap())
        .collect()
}

pub fn get_all() -> Vec<PersonalRecord> {
    let sql = "SELECT * FROM personal_record";
    get_multiple(sql, params![])
}

pub fn get_by_owner_id(owner_id: i64) -> Vec<PersonalRecord> {
    let sql = "SELECT * FROM personal_record WHERE owner_id = ?";
    get_multiple(sql, params![owner_id])
}