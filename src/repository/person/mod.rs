use std::mem;

use rusqlite::{params, Statement};
use serde::{Serialize, Deserialize};

use super::{get_connection, EntityError};

#[derive(Debug, Serialize, Deserialize)]
pub struct Person {
    pub id: u64,
    pub name: String,
    pub age: u32,
}

pub fn add_people(people: &[Person]) -> rusqlite::Result<()> {
    let mut con = get_connection();
    let tx = con.transaction()?;
    let sql = "INSERT INTO person (name, age) VALUES (?, ?)";
    let mut stmt = tx.prepare(sql)?;
    for person in people {
        stmt.insert(params![&person.name, person.age])?;
    }
    mem::drop(stmt);
    tx.commit()
}

pub fn add(person: &Person) -> Result<i64, EntityError> {
    let con = get_connection();
    let mut stmt: Statement = con.prepare("INSERT INTO person (name, age) VALUES (?, ?)").unwrap();
    match stmt.insert(params![&person.name, person.age]) {
        Ok(id) => Ok(id),
        Err(_) => Err(EntityError::AlreadyExists)
    }
}

pub fn delete_by_id(id: i64) -> Result<(), EntityError> {
    let con = get_connection();
    let mut stmt: Statement = con.prepare("DELETE FROM person WHERE id = ?").unwrap();
    match stmt.execute(params![id]) {
        Ok(1) => Ok(()),
        _ => Err(EntityError::NotExist)
    }
}

pub fn create_table() {
    let con = get_connection();
    con.execute(
        r#"
            CREATE TABLE IF NOT EXISTS person (
                id INTEGER PRIMARY KEY, 
                name TEXT NOT NULL UNIQUE, 
                age INTEGER NOT NULL
            )
        "#, 
        params![])
    .unwrap();
}


pub fn get_all() -> Vec<Person> {
    let con = get_connection();
    let mut stmt: Statement = con.prepare("SELECT * FROM person").unwrap();
    let people: Vec<Person> = stmt.query_map(params![], |row| {
        Ok(Person {
            id: row.get("id")?,
            name: row.get("name")?,
            age: row.get("age")?
        })
    })
    .unwrap()
    .map(|person| person.unwrap())
    .collect();
    people
}

pub fn delete_all() -> rusqlite::Result<usize> {
    let con = get_connection();
    con.execute("DELETE FROM person", params![])
}