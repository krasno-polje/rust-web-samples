// Это всё не настоящие тесты

use super::{person::{Person, self}, init_db};

#[test]
fn test_init_db() {
    init_db();
}

#[test]
fn test_add_people() {
    let people: Vec<Person> = vec![
        Person { id: 0, name: String::from("Alex"), age: 22 },
        Person { id: 0, name: String::from("David"), age: 23 },
        Person { id: 0, name: String::from("Michael"), age: 24 },
    ];
    person::add_people(&people).unwrap();
}

#[test]
fn test_get_all_people() {
    let people: Vec<Person> = person::get_all();
    if people.is_empty() {
        println!("There are no people");
        return;
    }
    println!("People:");
    println!();
    for person in people {
        println!("{:?}", person);
    }
}

#[test]
fn test_delete_all_people() {
    let deleted_count: usize = person::delete_all().unwrap();
    println!("People deleted: {}", deleted_count);
}