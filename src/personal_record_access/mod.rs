use crate::{tokens::{self, JwtClaims}, entity::personal_record::PersonalRecord, repository::personal_record};

#[cfg(test)]
mod tests;

#[derive(Debug)]
pub enum TokenError {
    RefreshRequired,
    SomeError
}

pub fn new_record(token: &str, message: &str) -> Result<PersonalRecord, TokenError> {
    match token_to_claims(token) {
        Ok(claims) => {
            let owner_id: i64 = claims.sub;
            personal_record::create(owner_id, message)
                .map_err(|_| TokenError::SomeError)
        },
        Err(err) => Err(err)
    }
}

fn token_to_claims(token: &str) -> Result<JwtClaims, TokenError> {
    tokens::parse_token(token)
        .map_err(|err| {
            match err {
                tokens::TokenParsingError::TokenExpired => TokenError::RefreshRequired,
                _ => TokenError::SomeError
            }
        })
}

pub fn get_all_records(token: &str) -> Result<Vec<PersonalRecord>, TokenError> { 
    match token_to_claims(token) {
        Ok(claims) => {
            let owner_id: i64 = claims.sub;
            Ok(personal_record::get_by_owner_id(owner_id))
        },
        Err(err) => Err(err)
    }
}