use super::{sign_up, sign_in};

#[test]
fn test_sign_up() {
    let username = "some_username";
    let password = "some pas$w0rd";
    match &sign_up(username, password) {
        Ok(_) => {
            println!("Success!");
        },
        Err(err) => {
            dbg!(err);
        }
    }
}

#[test]
fn test_sign_in() {
    let username = "some_username";
    let password = "some pas$w0rd";
    match &sign_in(username, password) {
        Ok(token) => {
            dbg!(token);
        },
        Err(err) => {
            dbg!(err);
        }
    }
}