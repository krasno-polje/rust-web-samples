use crate::{
    repository, 
    hash::{
        self, 
        verify_password_hash
    }, 
    entity::account::{Account, UserRole}, 
    tokens::{self, TokenParsingError, JwtClaims}
};

#[cfg(test)]
mod tests;

#[derive(Debug)]
pub enum SignUpError {
    AlreadyExists,
    Other
}

pub fn sign_up(username: &str, password: &str) -> Result<(), SignUpError> {
    if repository::account::exists_by_name(username) {
        return Err(SignUpError::AlreadyExists);
    }
    let pwd_hash: String = hash::hash_password(password);
    repository::account::create(username, &pwd_hash)
        .map(|_| ())
        .map_err(|_| SignUpError::Other)
}

#[allow(dead_code)]
#[derive(Debug)]
pub enum SignInError {
    NotExists,
    IncorrectPassword,
    Other
}

/// Returns an authentication token or an error
///
/// # Errors
///
/// This function will return an error if .
pub fn sign_in(username: &str, password: &str) -> Result<String, SignInError> {
    let account: Option<Account> = repository::account::get_by_name(username);
    if account.is_none() {
        return Err(SignInError::NotExists);
    }
    let account: Account = account.unwrap();
    if !verify_password_hash(password, &account.password_hash) {
        return Err(SignInError::IncorrectPassword);
    }
    Ok(tokens::create_refresh_token(account.id, &account.role))
}

pub enum Permission {
    HasRole(UserRole),

}

pub enum PermissionCheckResult {
    Ok(bool),
    RefreshRequired,
    SomeError
}

pub fn check_permission(token: &str, permission: &Permission) -> PermissionCheckResult {
    match &tokens::parse_token(token) {
        Ok(claims) => claims_match_permission(claims, permission),
        Err(TokenParsingError::TokenExpired) => PermissionCheckResult::RefreshRequired,
        Err(_) => PermissionCheckResult::SomeError
    }
}

fn claims_match_permission(claims: &JwtClaims, permission: &Permission) -> PermissionCheckResult {
    match permission {
        Permission::HasRole(role) => PermissionCheckResult::Ok(claims.role == *role)
    }
}

#[allow(dead_code)]
pub fn all_permissions_match(token: &str, permissions: &[Permission]) -> PermissionCheckResult {
    match &tokens::parse_token(token) {
        Ok(claims) => {
            let mut all_match: bool = true;
            for permission in permissions {
                match &claims_match_permission(claims, permission) {
                    PermissionCheckResult::Ok(false) => {
                        all_match = false;
                        break;
                    },
                    PermissionCheckResult::Ok(true) | _ => {},
                }
            }
            PermissionCheckResult::Ok(all_match)
        },
        Err(TokenParsingError::TokenExpired) => PermissionCheckResult::RefreshRequired,
        Err(_) => PermissionCheckResult::SomeError
    }
}

#[allow(dead_code)]
#[derive(Debug)]
pub enum TokenValidation {
    RefreshRequired,
    Valid,
    Invalid
}

#[allow(dead_code)]
pub fn validate_token(token: &str) -> TokenValidation {
    match tokens::parse_token(token) {
        Ok(_) => TokenValidation::Valid, 
        Err(TokenParsingError::TokenExpired) => TokenValidation::RefreshRequired,
        Err(_) => TokenValidation::Invalid
    }
}