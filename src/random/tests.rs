use super::gen_rand_bytes;

#[test]
fn test_rand() {
    let rand_bytes: Vec<u8> = gen_rand_bytes(32);
    let rand_bytes_hex: String = hex::encode_upper(&rand_bytes);
    dbg!(rand_bytes_hex);
}