use std::vec;
use rand::RngCore;
use rand_core::OsRng;

#[cfg(test)]
mod tests;

pub fn gen_rand_bytes(length: usize) -> Vec<u8> {
    let mut bytes: Vec<u8> = vec![0u8; length];
    OsRng.fill_bytes(&mut bytes);
    bytes
}