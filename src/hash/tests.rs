use argon2::{Variant, Version, ThreadMode};

use crate::random::gen_rand_bytes;

#[test]
fn argon2_test() {
    let config = argon2::Config {
        variant: Variant::Argon2id,
        ad: &[],
        hash_length: 32,
        lanes: 4,
        mem_cost: 65536,
        secret: &[],
        thread_mode: ThreadMode::Parallel,
        time_cost: 10,
        version: Version::Version13,
    };
    let password = "привет, мир!";
    let salt: Vec<u8> = gen_rand_bytes(16);
    let hash: Vec<u8> = argon2::hash_raw(password.as_bytes(), &salt, &config).unwrap();
    dbg!(hex::encode_upper(&hash));
}