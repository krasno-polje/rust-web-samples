use argon2::{Variant, Version, ThreadMode};

use crate::random::gen_rand_bytes;

#[cfg(test)]
mod tests;

pub fn hash_password(password: &str) -> String {
    static CONFIG: argon2::Config<'static> = argon2::Config {
        variant: Variant::Argon2id,
        ad: &[],
        hash_length: 32,
        lanes: 4,
        mem_cost: 65536,
        secret: &[],
        thread_mode: ThreadMode::Parallel,
        time_cost: 10,
        version: Version::Version13,
    };
    let salt: Vec<u8> = gen_rand_bytes(16);
    argon2::hash_encoded(password.as_bytes(), &salt, &CONFIG).unwrap()
}

pub fn verify_password_hash(pwd_variant: &str, hash: &str) -> bool {
    argon2::verify_encoded(hash, pwd_variant.as_bytes()).unwrap()
}