use std::time::{SystemTime, UNIX_EPOCH};

use jwt::{Header, EncodingKey, Validation, DecodingKey};
use serde::{Serialize, Deserialize};

use crate::entity::account::UserRole;

extern crate jsonwebtoken as jwt;

#[cfg(test)]
mod tests;

#[derive(Serialize, Deserialize, Debug)]
pub struct JwtClaims {
    pub sub: i64,
    pub iat: u64,
    pub exp: u64,
    pub role: UserRole
}

impl JwtClaims {
    fn new(id: i64, expiration_in_secs: u64, role: &UserRole) -> Self {
        let current_time_secs = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_secs();
        Self {
            sub: id, 
            iat: current_time_secs, 
            exp: current_time_secs + expiration_in_secs,
            role: role.clone()
        }
    }
}

pub fn create_refresh_token(id: i64, role: &UserRole) -> String {
    static REFRESH_TOKEN_EXPIRES_IN_SECS: u64 = 60 * 60 * 24 * 30; // 30 days
    create_token(id, role, REFRESH_TOKEN_EXPIRES_IN_SECS)
}

fn create_access_token(id: i64, role: &UserRole) -> String {
    static ACCESS_TOKEN_EXPIRES_IN_SECS: u64 = 5 * 60; // 5 minutes
    create_token(id, role, ACCESS_TOKEN_EXPIRES_IN_SECS)
}

fn create_token(id: i64, role: &UserRole, expires_in: u64) -> String {
    let key = EncodingKey::from_secret("ocenn-sekretny1-klyuc".as_ref());
    let claims = JwtClaims::new(id, expires_in, role);
    let header = Header::new(jwt::Algorithm::HS256);
    jwt::encode(&header, &claims, &key).unwrap()
}

pub enum TokenParsingError {
    InvalidToken,
    TokenExpired,
    MissingClaim(String),
    Other
}

pub fn parse_token(token: &str) -> Result<JwtClaims, TokenParsingError> {
    let key = DecodingKey::from_secret("ocenn-sekretny1-klyuc".as_ref());
    let validation = Validation::new(jwt::Algorithm::HS256);
    jwt::decode::<JwtClaims>(token, &key, &validation)
        .map(|token| token.claims)
        .map_err(|err| {
            match err.kind() {
                jwt::errors::ErrorKind::ExpiredSignature => TokenParsingError::TokenExpired,
                jwt::errors::ErrorKind::InvalidSignature => TokenParsingError::InvalidToken,
                jwt::errors::ErrorKind::MissingRequiredClaim(claim_name) 
                    => TokenParsingError::MissingClaim(claim_name.clone()),
                _ => TokenParsingError::Other
            }
        })
}

#[derive(Debug)]
pub struct TokensPair {
    access_token: String,
    refresh_token: String
}

pub fn create_tokens_pair(id: i64, role: &UserRole) -> TokensPair {
    let refresh_token: String = create_refresh_token(id, role);
    let access_token: String = create_access_token(id, role);
    TokensPair { access_token, refresh_token }
}

pub fn refresh(refresh_token: &str) -> Result<String, TokenParsingError> {
    let token_claims: JwtClaims = parse_token(refresh_token)?;
    let access_token: String = create_access_token(token_claims.sub, &token_claims.role);
    Ok(access_token)
}