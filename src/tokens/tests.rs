extern crate jsonwebtoken as jwt;

use std::{time::{SystemTime, UNIX_EPOCH}, str::FromStr};
use jwt::{EncodingKey, Header, DecodingKey, Validation};

use crate::tokens::JwtClaims;

use super::{UserRole, create_refresh_token};

#[test]
fn test_jwt() {
    let current_time: u64 = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_secs();
    let claims = JwtClaims {
        sub: 20,
        iat: current_time,
        exp: current_time + 10,
        role: UserRole::User
    };
    let key = EncodingKey::from_secret("super secret password".as_ref());
    let token: String = jwt::encode(&Header::default(), &claims, &key).unwrap();
    dbg!(&token);
}

#[test]
fn test_user_role_str() {
    let role = UserRole::Admin;
    println!("Role: {}", role);
    println!("Role from string: {}", UserRole::from_str("Moder").unwrap());
}

#[test]
fn test_token_parsing() {
    let token_str = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIwLCJpYXQiOjE2NjQ3MDg5NDcsImV4cCI6MTY2NDcwODk1Nywicm9sZSI6IlVzZXIifQ.JtKHqWtwFscF-BawPLlmUj7ZC5YX_5_BQ7jpZtu2dA8";
    let key = DecodingKey::from_secret("super secret password".as_ref());
    let validation = Validation::new(jwt::Algorithm::HS256);
    let result: Result<JwtClaims, jwt::errors::Error> = jwt::decode(token_str, &key, &validation)
        .map(|token| token.claims);
    match &result {
        Ok(claims) => {
            dbg!(claims);
        },
        Err(err) => {
            match err.kind() {
                jwt::errors::ErrorKind::ExpiredSignature => {
                    println!("Signature expired");
                },
                jwt::errors::ErrorKind::InvalidSignature => {
                    println!("Invalid signature. Probably you passed an incorrect password")
                },
                _ => {
                    println!("Some other type error")
                }
            }
        }
    }
    // let token = parse_token(token_str).unwrap();
    // dbg!(result);
}

#[test]
fn test_create_refresh_token() {
    let token: String = create_refresh_token(123, &UserRole::Admin);
    println!("Refresh token: {}", token);
}