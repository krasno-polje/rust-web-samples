use serde::Serialize;

#[cfg(test)]
mod tests;

#[derive(Debug, Serialize)]
pub struct PersonalRecord {
    pub id: i64,
    pub owner_id: i64,
    pub text: String
}

impl PersonalRecord {
    pub fn new(id: i64, owner_id: i64, message: &str) -> Self {
        Self { 
            id, 
            owner_id, 
            text: message.to_owned()
        }
    }
}