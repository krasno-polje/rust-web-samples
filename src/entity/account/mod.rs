use serde::{Serialize, Deserialize};
use strum_macros::{Display, EnumString};

mod tests;

#[derive(Clone, Serialize, Deserialize, EnumString, Display, Debug, PartialEq)]
pub enum UserRole {
    User,
    Admin,
    Moder
}

#[derive(Debug, Clone)]
pub struct Account {
    pub id: i64,
    pub name: String,
    pub password_hash: String,
    pub role: UserRole
}

impl Account {
    pub fn new(id: i64, name: &str, password_hash: &str, role: &UserRole) -> Self {
        Self {
            id, 
            name: name.to_owned(), 
            password_hash: password_hash.to_owned(),
            role: role.clone() 
        }
    }
}