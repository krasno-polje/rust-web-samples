use std::{
    fmt::Debug, 
    sync::Mutex, 
};

use actix_files::Files;
use actix_web::{
    HttpServer, 
    Responder, 
    HttpResponse, 
    HttpRequest, 
    get, 
    App, 
    post, 
    web, 
    http::header, cookie::{Cookie, SameSite}
};
use auth::{SignUpError, Permission};
use entity::account::UserRole;
use handlebars::{
    Handlebars, 
    JsonValue
};
use serde::Deserialize;
use serde_json::json;

use crate::{
    repository::person::Person, 
    auth::{
        SignInError, 
        PermissionCheckResult
    }, 
    entity::personal_record::PersonalRecord
};

extern crate jsonwebtoken as jwt;

#[allow(dead_code)]
mod repository;
#[allow(dead_code)]
mod tokens;
mod auth;
mod entity;
mod hash;
mod random;
mod personal_record_access;

static HOST: &str = "127.0.0.1";

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    repository::init_db();
    let handlebars_ref = configure_handlebars();
    let port: u16 = load_port();
    println!("Starting server on port {}...", port);
    HttpServer::new(move || {
        App::new()
            .app_data(handlebars_ref.clone())
            .service(add_person)
            .service(delete_person)
            .service(get_index_endpoint)
            .service(get_auth_endpoint)
            .service(sign_up_endpoint)
            .service(sign_in_endpoint)
            .service(get_records_page)
            .service(add_record)
            .service(get_admin_endpoint)
            .service(set_user_role_admin)
            .service(logout)
            .service(Files::new("/", "./static").show_files_listing())
    })
    .bind((HOST, port))?
    .run()
    .await
}

fn load_port() -> u16 {
    static DEFAULT_PORT: u16 = 8085;
    static LAZY_PORT: Mutex<Option<u16>> = Mutex::new(None);
    let mut unlocked_port = LAZY_PORT.lock().unwrap();
    if let Some(port) = *unlocked_port {
        return port;
    }
    let port = match dotenv::var("PORT")
        .ok()
        .and_then(|port_str| u16::from_str_radix(&port_str, 10).ok())
    {
        Some(port_from_env) => port_from_env,
        None => DEFAULT_PORT
    };
    *unlocked_port.insert(port)
}

fn configure_handlebars() -> web::Data<Handlebars<'static>> {
    let mut handlebars = Handlebars::new();
    handlebars
        .register_templates_directory(".hbs", "static/templates")
        .unwrap();
    let handlebars_ref = web::Data::new(handlebars);
    handlebars_ref
}

#[derive(Deserialize)]
struct ErrorQuery {
    error: String
}

impl Debug for ErrorQuery {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("ErrorQuery").field("error", &self.error).finish()
    }
}

// vozmožno, nužno budet pomenätt PermissionCheckResult na Result<bool, PermissionCheckError>
#[get("/admin")]
async fn get_admin_endpoint(hb: web::Data<Handlebars<'_>>, req: HttpRequest) -> HttpResponse {
    let auth_token: String = match extract_auth_token(&req) {
        Some(token) => token,
        None => return redirect_to_auth()
    };
    let has_role_admin = Permission::HasRole(UserRole::Admin);
    match auth::check_permission(&auth_token, &has_role_admin) {
        PermissionCheckResult::Ok(true) => get_admin_page(hb),
        PermissionCheckResult::Ok(false) => redirect_to_records(),
        _ => redirect_to_auth_no_cookies(hb)
    }
}

#[get("/set-admin")]
async fn set_user_role_admin(hb: web::Data<Handlebars<'_>>, req: HttpRequest) -> HttpResponse {
    static CHECK_ROLE: bool = false;
    match extract_auth_token(&req) {
        Some(token) => {
            if CHECK_ROLE {
                let has_role_admin = &Permission::HasRole(UserRole::Admin);
                match auth::check_permission(&token, &has_role_admin) {
                    PermissionCheckResult::Ok(false)
                        | PermissionCheckResult::RefreshRequired
                        | PermissionCheckResult::SomeError => return redirect_to_records(),
                    _ => {}
                }
            }
        },
        None => return redirect_to_auth()
    }
    let username: String = match extract_username_for_set_role(&req) {
        Some(username) => username,
        None => return redirect_to_records()
    };
    let account_not_exists: bool = !repository::account::set_role_by_username(&username, &UserRole::Admin);
    if account_not_exists {
        return set_admin_not_exist_page(hb, &username);
    }
    set_admin_success_page(hb, &username)
}

fn set_role_redirect_json(message: &str) -> JsonValue {
    json!({
        "redirect": {
            "isPresent": true,
            "path": "records",
            "delay": {
                "isPresent": true,
                "secs": 10
            },
        },
        "message": message,
        "host": HOST,
        "port": load_port()
    })
}

fn set_admin_success_page(hb: web::Data<Handlebars<'_>>, username: &str) -> HttpResponse {
    let message = format!("Пользователь {} успешно назначен администратором", username);
    let data: JsonValue = set_role_redirect_json(&message);
    let body: String = hb.render("set-admin", &data).unwrap();
    HttpResponse::Ok().body(body)
}

fn set_admin_not_exist_page(hb: web::Data<Handlebars<'_>>, username: &str) -> HttpResponse {
    let message = format!("Пользователя с именем {} не существует", username);
    let data: JsonValue = set_role_redirect_json(&message);
    let body: String = hb.render("set-admin", &data).unwrap();
    HttpResponse::Ok().body(body)
}

fn extract_username_for_set_role(req: &HttpRequest) -> Option<String> {
    let query_params: Option<Vec<(String, String)>> = serde_urlencoded::from_str(req.query_string()).ok();
    if let Some(params) = query_params {
        params.into_iter()
            .find(|(key, _)| key == "username")
            .map(|(_, value)| value)
    } else {
        None
    }
}

fn get_admin_page(hb: web::Data<Handlebars<'_>>) -> HttpResponse {
    let body: String = hb.render("admin", &json!({})).unwrap();
    HttpResponse::Ok().body(body)
}

#[get("/")]
async fn get_index_endpoint(hb: web::Data<Handlebars<'_>>, req: HttpRequest) -> HttpResponse {
    if !req.query_string().is_empty() {
        if let Some(err_query) = parse_error_query(req.query_string()) {
            return get_index_with_err(hb, &ApiError::new(err_query.error.as_str()));
        }
    } 
    get_index_page(hb)
}

#[get("/auth")]
async fn get_auth_endpoint(hb: web::Data<Handlebars<'_>>, req: HttpRequest) -> HttpResponse {
    if extract_auth_token(&req).is_some() {
        return redirect_to_records();
    }
    if !req.query_string().is_empty() {
        if let Some(params) = parse_auth_error_query(req.query_string()) {
            let (key, message) = &params[0];
            match key.as_str() {
                "signUpErr" => return render_auth_with_errors(
                    hb, Some(&ApiError::new(&message)), None),
                "signInErr" => return render_auth_with_errors(
                    hb, None, Some(&ApiError::new(&message))),
                _ => {}
            }
        }
    } 
    get_auth_page(hb)
}

#[get("/logout")]
async fn logout(hb: web::Data<Handlebars<'_>>, req: HttpRequest) -> HttpResponse {
    if extract_auth_token(&req).is_some() {
        return redirect_to_auth_no_cookies(hb);
    }
    redirect_to_auth()
}

fn parse_auth_error_query(query_str: &str) -> Option<Vec<(String, String)>> {
    serde_urlencoded::from_str(query_str).ok()
}

fn extract_auth_token(req: &HttpRequest) -> Option<String> {
    req.cookie("auth_token")
        .map(|cookie| cookie.value().to_string())
}

#[get("/records")]
async fn get_records_page(hb: web::Data<Handlebars<'_>>, req: HttpRequest) -> HttpResponse {
    let token: String = match extract_auth_token(&req) {
        Some(token) => token,
        None => return redirect_to_auth()
    };
    let records: Vec<PersonalRecord> = match personal_record_access::get_all_records(&token) {
        Ok(records) => records,
        Err(_) => return redirect_to_auth_no_cookies(hb)
    };
    let body: String = render_records_page(hb, &records);
    HttpResponse::Ok().body(body)
}

#[derive(Debug, Deserialize)]
struct AddRecordForm {
    text: String
}

#[post("/record")]
async fn add_record(hb: web::Data<Handlebars<'_>>, 
                    web::Form(record_form): web::Form<AddRecordForm>, 
                    req: HttpRequest) -> HttpResponse {
    let message: String = record_form.text;
    let token: String = match extract_auth_token(&req) {
        Some(token) => token,
        None => return get_auth_page(hb)
    };
    if personal_record_access::new_record(&token, &message).is_err() {
        return redirect_to_auth_no_cookies(hb);
    }
    redirect_to_records()
}

fn redirect_to_records() -> HttpResponse {
    HttpResponse::SeeOther()
        .append_header((header::LOCATION, "/records"))
        .finish()
}

fn render_records_page(hb: web::Data<Handlebars<'_>>, records: &[PersonalRecord]) -> String {
    let data = if records.is_empty() {
        json!({
            "recordsArePresent": false,
            "host": HOST,
            "port": load_port()
        })
    } else {
        json!({
            "recordsArePresent": true,
            "records": records,
            "host": HOST,
            "port": load_port()
        })
    };
    hb.render("records", &data).unwrap()
}

#[derive(Deserialize, Debug)]
struct SignUpForm {
    username: String,
    password: String,
    repeated_password: String
}

#[post("/sign-up")]
async fn sign_up_endpoint(form: web::Form<SignUpForm>) -> HttpResponse {
    if form.password != form.repeated_password {
        return signup_redirect_with_err("Пароль в обоих полях должен быть один и тот же.");
    }
    match auth::sign_up(&form.username, &form.password) {
        Err(SignUpError::AlreadyExists) => 
            return signup_redirect_with_err("Аккаунт с таким именем уже существует"),
        Err(SignUpError::Other) =>
            return signup_redirect_with_err("Неизвестная ошибка. Обратитесь к администрации."),
        _ => {}
    }
    HttpResponse::SeeOther()
        .append_header((header::LOCATION, "/auth"))
        .finish()
}

#[derive(Debug, Deserialize)]
pub struct SignInForm {
    username: String,
    password: String
}

#[post("/sign-in")]
async fn sign_in_endpoint(form: web::Form<SignInForm>) -> HttpResponse {
    match auth::sign_in(&form.username, &form.password) {
        Ok(token) => {
            redirect_to_records_with_token(&token)
        },
        Err(SignInError::NotExists) => return signin_redirect_with_err(
            "Пользователь с таким именем не зарегистрирован."),
        Err(SignInError::IncorrectPassword) => return signin_redirect_with_err("Неверный пароль."),
        _ => return signin_redirect_with_err("Неизвестная ошибка. Обратитесь к администрации.")
    }
}

// todo: 
//       Straniću dlä licnyh zapisei.
//       Perenapravlenie na nejė pri pravillnom vhode.
//       Polucenie zapisei s proverkoi tokena. Ih spisok s ID dolžen nahodittsä v tabliće,
//           i tam že ssylki na stranićy, každaja iz kotoryh otobražajet licnuju zapiss.
//       Tocku vhoda dlä polucenija stranićy s licnoi zapiśju s proverkoi tokena.
//       So vremenem licnyie zapisi možno budet peredelatt v posty bloga s Markdown i ostallnoi sait pod blog.
//       Tocku vhoda i knopku dlä vyhoda iz akkaunta putėm udalenija cookie.

fn signup_redirect_with_err(err_msg: &str) -> HttpResponse {
    auth_redirect_with_err("signUpErr", err_msg)
}

fn signin_redirect_with_err(err_msg: &str) -> HttpResponse {
    auth_redirect_with_err("signInErr", err_msg)
}

fn auth_redirect_with_err(query_key: &str, err_msg: &str) -> HttpResponse {
    let query_params: String = encode_error_query_param(query_key, err_msg);
    HttpResponse::SeeOther()
        .append_header((header::LOCATION, format!("/auth?{}", query_params)))
        .finish()
}

fn redirect_to_auth() -> HttpResponse {
    HttpResponse::SeeOther()
        .append_header((header::LOCATION, "/auth"))
        .finish()
}

fn redirect_to_records_with_token(token: &str) -> HttpResponse {
    HttpResponse::SeeOther()
        .append_header((header::LOCATION, "/auth"))
        .cookie(
            Cookie::build("auth_token", token)
                .http_only(true)
                .secure(true)
                .same_site(SameSite::Strict)
                .finish()
        )
        .finish()
}

fn encode_error_query_param(key: &str, err_msg: &str) -> String {
    serde_urlencoded::to_string(&[(key, err_msg)]).unwrap()
}

fn parse_error_query(query_str: &str) -> Option<ErrorQuery> {
    match serde_urlencoded::from_str::<ErrorQuery>(query_str) {
        Ok(error_query) => Some(error_query),
        Err(_) => None
    }
}
 
struct ApiError {
    message: String
}

impl Clone for ApiError {
    fn clone(&self) -> Self {
        Self { message: self.message.clone() }
    }
}

impl ApiError {
    fn new(message: &str) -> Self {
        Self { message: message.to_string() }
    }
}

fn get_index_page(hb: web::Data<Handlebars<'_>>) -> HttpResponse {
    let no_error: Option<ApiError> = None;
    let people_json = get_people_json(&no_error);
    render_index_from_json(hb, &people_json)
}

fn get_index_with_err(hb: web::Data<Handlebars<'_>>, error: &ApiError) -> HttpResponse {
    let people_json = get_people_json(&Some(error.clone()));
    render_index_from_json(hb, &people_json)
}

fn render_auth_page(hb: web::Data<Handlebars<'_>>) -> String {
    let auth_json = json!({
        "errorIsPresent": false,
        "host": "127.0.0.1",
        "port": load_port()
    });
    hb.render("auth", &auth_json).unwrap()
}

fn get_auth_page(hb: web::Data<Handlebars<'_>>) -> HttpResponse {
    HttpResponse::Ok().body(render_auth_page(hb))
}

fn redirect_to_auth_no_cookies(hb: web::Data<Handlebars<'_>>) -> HttpResponse {
    let mut cookie = Cookie::new("auth_token", "");
    cookie.make_removal();
    HttpResponse::SeeOther()
        .append_header((header::LOCATION, "/auth"))
        .cookie(cookie)
        .body(render_auth_page(hb))
}

fn api_err_to_json(api_err: Option<&ApiError>) -> JsonValue {
    match api_err {
        Some(err) => json!({
            "isPresent": true,
            "message": err.message
        }),
        None => json!({
            "isPresent": false
        })
    }
}

fn render_auth_with_errors(hb: web::Data<Handlebars<'_>>, 
                           signup_err: Option<&ApiError>, signin_err: Option<&ApiError>) -> HttpResponse {
    let auth_json = json!({
        "signupError": api_err_to_json(signup_err),
        "signinError": api_err_to_json(signin_err),
        "host": HOST,
        "port": load_port()
    });
    let body: String = hb.render("auth", &auth_json).unwrap();
    HttpResponse::Ok().body(body)
}

fn get_people_json(error: &Option<ApiError>) -> JsonValue {
    let people: Vec<Person> = repository::person::get_all();
    if let Some(err) = error {
        return json!({ 
            "people": people, 
            "anyExist": !people.is_empty(),
            "errorIsPresent": true,
            "errorMessage": err.message
        });
    }
    json!({ 
        "people": people, 
        "anyExist": !people.is_empty(),
        "errorIsPresent": false
    })
}

fn render_index_from_json(hb: web::Data<Handlebars<'_>>, json_value: &JsonValue) -> HttpResponse {
    let body: String = hb.render("index", json_value).unwrap();
    HttpResponse::Ok().body(body)
}

#[derive(Deserialize)]
struct PersonForm {
    name: String,
    age: Option<u32>,
}

fn form_to_person(form: &PersonForm) -> Person {
    Person { 
        id: 0, 
        name: form.name.clone(), 
        age: form.age.unwrap() 
    }
}

#[post("/")]
async fn add_person(form: web::Form<PersonForm>, hb: web::Data<Handlebars<'_>>) -> impl Responder {
    if let None = form.age {
        println!("Age is empty");
        return get_index_page(hb);
    }
    match repository::person::add(&form_to_person(&form)) {
        Ok(_) => {
            
        }
        Err(_) => {

        }
    }
    get_index_page(hb)
}

#[derive(Deserialize)]
struct DeletePersonForm {
    id: i64
}

#[post("/delete")]
async fn delete_person(form: web::Form<DeletePersonForm>) -> impl Responder {
    match repository::person::delete_by_id(form.id) {
        Ok(_) => {
            
        }
        Err(_) => {
            let err_msg: String = format!("Человека с id {} нет в базе", form.id);
            let url_values = &[("error", err_msg)];
            let err_msg_urlencoded: String = serde_urlencoded::to_string(url_values).unwrap();
            return HttpResponse::SeeOther()
                .append_header((header::LOCATION, format!("/?{}", err_msg_urlencoded)))
                .finish()
        }
    }
    HttpResponse::SeeOther()
        .append_header((header::LOCATION, "/"))
        .finish()
}